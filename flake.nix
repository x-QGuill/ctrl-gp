{
  description = "A very basic flake";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-22.05";
  };

  outputs = { self, nixpkgs }:
  let
    system = "x86_64-linux";
    pkgs = import nixpkgs { inherit system; };
  in
  {
    devShells.${system} = rec {
      default = julia;
      julia = pkgs.mkShell {
        buildInputs = [
          pkgs.julia-stable-bin
        ];
        shellHook = ''
          julia -e '
            using Pkg;
            using Pluto;
            Pluto.run(notebook=joinpath(pwd(), "notebook.jl"))'
          '';
      };
    };
  };
}
